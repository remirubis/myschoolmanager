const express = require('express');
const Multer = require('multer');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');
const { v4: uuidv4 } = require('uuid');

const app = express();
const port = 3000;

let filename = '';

const storage = Multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, '../src/assets/public/');
  },
  filename: (req, file, cb) => {
    filename = uuidv4() + path.extname(file.originalname).toLowerCase();
    cb(null, filename);
  }
});

const upload = Multer({ storage });

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(upload.any());

app.post('/upload', upload.single('picture'), (req, res) => res.status(200).send({ filename }));

app.listen(port);
