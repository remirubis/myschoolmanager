import CardComponent from '../components/Card';

export default class Homepage {
  constructor(db) {
    this.app = document.getElementById('content');
    this.db = db;
    this.nbStudents = this.db.getSize('students');
    this.currentPage = 1;
    this.elementPerPage = 5;
    this.elementPerPageProposal = [5, 10, 25];
    this.header = [
      {
        content: 'Nom',
        size: 3
      },
      {
        content: 'Prénom',
        size: 3
      },
      {
        content: 'Promotion',
        size: 1
      },
      {
        content: 'Spécialité',
        size: 4
      }
    ];
    this.run();
  }

  async getStatistics() {
    this.nbStudents = await this.db.getSize('students');
    const nbPromotions = await this.db.getSize('promotions');
    const lastConnections = await this.db.sortDataBy('students', 'updated_at').then((res) => res.slice(0, 3));
    let connectionsEl = '';
    lastConnections.forEach((user) => {
      const lastConnection = new Date(user.updated_at * 1000).toLocaleDateString('fr-FR');
      connectionsEl += `
        <li class="list-group-item d-flex w-100 justify-content-between align-items-center">
          <p class="mb-1">${user.firstname}</p>
          <small class="fs-7">${lastConnection}</small>
        </li>
      `;
    });
    const data = [
      {
        title: 'Nombre d\'étudiants',
        content: `<p class="fs-1 fw-bold mb-0 text-end p-3">${this.nbStudents}</p>`,
        footer: {
          content: 'Liste des étudiants',
          link: '/etudiants'
        }
      }, {
        title: 'Nombre de promotions',
        content: `<p class="fs-1 fw-bold mb-0 text-end p-3">${nbPromotions}</p>`,
        footer: {
          content: 'Liste des promotions',
          link: '/promotions'
        }
      }, {
        title: 'Dernières activités',
        content: `
          <ul class="list-group list-group-flush h-100">
            ${connectionsEl}
          </ul>
        `,
        footer: null
      }
    ];
    let el = '';
    await data.forEach((entry) => {
      el += `
    <div class="col">
  ${new CardComponent(
    entry.title,
    entry.content,
    entry.footer ? `<a href="${entry.footer.link}" class="text-black-50 fs-7 router">${entry.footer.content}</a>` : null
  ).display()}
    </div>`;
    });
    return el;
  }

  async renderTable(head, body) {
    let columns = [];
    let rows = [];

    const startIndex = this.currentPage !== 1
      ? ((this.currentPage - 1) * this.elementPerPage)
      : 0;
    const minBody = await body.then(
      (res) => res.slice(startIndex, this.elementPerPage * this.currentPage)
    );
    head.forEach((header) => {
      columns += `<th scope="col-${header.size}" class="text-center">${header.content}</th>`;
    });
    await minBody.forEach((user) => {
      rows += `
        <tr class="align-middle text-center">
          <td>${user.lastname}</td>
          <td>${user.firstname}</td>
          <td>B3</td>
          <td>Développement web</td>
          <td class="d-flex justify-content-center">
            <a href="etudiant/${user.id}" class="btn btn-primary"><i class="fa-solid fa-eye"></i></a>
          </td>
        </tr>`;
    });
    return `
      <table class="table table-hover user-list" id="student-list">
        <thead class="table-dark">
          <tr>
            ${columns}
            <th scope="col-1" class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          ${await rows}
        </tbody>
      </table>
    `;
  }

  async renderPagination() {
    let pages = '';
    const students = await this.nbStudents;
    for (let i = 0; i < Math.ceil(students / this.elementPerPage); i += 1) {
      pages += `<li class="page-item ${this.currentPage === i + 1 ? 'active' : ''}" id="page-${i + 1}"><a class="page-link" href="#">${i + 1}</a></li>`;
    }
    return `
      <nav aria-label="Navigation des utilisateurs">
        <ul class="pagination justify-content-center">
          <li class="page-item">
            <a class="page-link" href="#" aria-label="Précédent" id="previous">
              &laquo;
            </a>
          </li>
          ${pages}
          <li class="page-item">
            <a class="page-link" href="#" aria-label="Suivant" id="next">
              &raquo;
            </a>
          </li>
        </ul>
      </nav>
    `;
  }

  async renderPage() {
    let elPerPage = '';
    this.elementPerPageProposal.forEach((el) => {
      elPerPage += `<option value="${el}" ${this.elementPerPage === el ? 'selected' : ''}>${el}</option>`;
    });
    return `
      <div class="container w-90 p-4" id="page">
        <div class="row row-cols-1 row-cols-md-3 g-4">
          ${await this.getStatistics()}
        </div>
        <!-- Tableau d'utilisateurs -->
        <div class="row mt-4">
          <div class="col">
            <div class="card overflow-hidden">
              ${await this.renderTable(this.header, this.db.getAllData('students'))}
              <div id="pagination">
                ${await this.renderPagination()}
              </div>
            </div>
          </div>
        </div>
        <div class="row mt-2">
          <div class="col-7"></div>
          <div class="col-2 d-flex align-items-center">
            <label for="elPerPage">Nombre d'entrés</label> 
          </div>
          <div class="col-3">
            <select class="form-select" id="elPerPage">
              ${elPerPage}
            </select>
          </div>
        </div>
      </div>
    `;
  }

  registerSelectEvent() {
    document.getElementById('elPerPage').addEventListener('change', async () => {
      this.elementPerPage = document.getElementById('elPerPage').value;
      document.getElementById('student-list').innerHTML = await this.renderTable(this.header, this.db.getAllData('students'));
      document.getElementById('pagination').innerHTML = await this.renderPagination();
    });
  }

  registerPaginationEvent() {
    document.body.onclick = async (e) => {
      if (e.target.className && e.target.className.indexOf('page-link') !== -1) {
        e.preventDefault();
        document.getElementById(`page-${this.currentPage}`).classList.remove('active');
        if (e.target.id === 'previous') {
          this.currentPage = this.currentPage === 1 ? this.currentPage : this.currentPage -= 1;
        } else if (e.target.id === 'next') {
          this.currentPage = this.currentPage === Math.ceil(this.nbStudents / this.elementPerPage)
            ? this.currentPage
            : this.currentPage += 1;
        } else {
          this.currentPage = parseInt(e.target.innerText, 10);
        }
        document.getElementById(`page-${this.currentPage}`).classList.add('active');
        document.getElementById('student-list').innerHTML = await this.renderTable(this.header, this.db.getAllData('students'));
      }
    };
  }

  async run() {
    this.app.innerHTML = await this.renderPage();
    this.registerSelectEvent();
    this.registerPaginationEvent();
  }
}
