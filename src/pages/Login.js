export default class Login {
  constructor(db, router) {
    this.app = document.getElementById('content');
    this.db = db;
    this.router = router;
    this.run();
  }

  renderPage() {
    return `
      <h2 class="mb-4">Connexion</h2>
      <div class="alert alert-info" role="alert">
        Vous n'avez pas encore de compte ? <a href="/register" class="router">Inscrivez-vous</a>
      </div>
      <form name="login">
        <div id="error"></div>
        <div class="form-group mb-2">
          <label for="email">Adresse email</label>
          <input type="email" class="form-control" id="email" placeholder="myschoolmanager@email.com">
        </div>
        <div class="form-group mb-2">
          <label for="password">Mot de passe</label>
          <input type="password" class="form-control" id="password" placeholder="*********">
        </div>
        <div class="form-check mb-3">
          <input type="checkbox" class="form-check-input" id="remember">
          <label class="form-check-label" for="remember">Se souvenir de moi</label>
        </div>
        <button type="submit" class="btn btn-primary">Se connecter</button>
      </form>
    `;
  }

  renderError(fields) {
    if (document.getElementById('errorMsg')) {
      document.getElementById('errorMsg').remove();
    }
    let errors = '';
    fields.forEach((field) => {
      errors += `<li>${field}</li>`;
    });
    return `
    <div class="alert alert-danger mt-4" role="alert" id="errorMsg">
      Une erreur est survenue sur les champs suivants :
      <ul class="mb-0">
        ${errors}
      </ul>
    </div>
    `;
  }

  loginFormSubmitEvent() {
    document.forms.login.onsubmit = (e) => {
      e.preventDefault();
      this.getFormData();
    };
  }

  async getFormData() {
    const errorFields = [];

    const data = {
      email: document.querySelector('#email'),
      password: document.querySelector('#password')
    };
    Object.entries(data).forEach((entry) => {
      if (entry[1].value === '' && !entry[1].checked) {
        errorFields.push(document.querySelector(`label[for="${entry[1].id}"]`).innerText);
        entry[1].classList.remove('is-valid');
        entry[1].classList.add('is-invalid');
      } else {
        entry[1].classList.remove('is-invalid');
        entry[1].classList.add('is-valid');
      }
    });

    if (errorFields.length !== 0) {
      document.getElementById('error').innerHTML = this.renderError(errorFields);
      return;
    }

    const payload = {
      email: data.email.value,
      password: this.db.hash(data.password.value)
    };

    const isExisted = await this.db.getAllData('students').then((users) => users.find((user) => {
      if (user.email === payload.email && user.password === payload.password) {
        return true;
      }
      return false;
    }));

    if (!isExisted) {
      data.email.classList.remove('is-valid');
      data.email.classList.add('is-invalid');
      data.password.classList.remove('is-valid');
      data.password.classList.remove('is-invalid');
      return;
    }

    localStorage.setItem('user', `${payload.firstname} ${payload.lastname}`);
    this.router.pushTo('/');
  }

  run() {
    this.app.innerHTML = this.renderPage();
    this.loginFormSubmitEvent();
  }
}
