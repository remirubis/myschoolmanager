const FormData = require('form-data');

export default class Register {
  constructor(db, router) {
    this.app = document.getElementById('content');
    this.genders = [{
      name: 'Homme',
      value: 'men'
    }, {
      name: 'Femme',
      value: 'female'
    }, {
      name: 'Autre',
      value: 'other'
    }];
    this.run();
    this.db = db;
    this.router = router;
  }

  renderPage() {
    let genders = '';
    this.genders.forEach((gender) => {
      genders += this.renderGenders(gender.name, gender.value, this.genders[0] === gender);
    });
    return `
      <h2 class="mb-4">Inscription</h2>
      <div class="alert alert-info" role="alert">
        Vous avez déjà un compte ? <a href="/login" class="router">Se connecter</a>
      </div>
      <form name="register">
        <div id="error"></div>
        <h4 class="mb-4">Général</h4>
        <div class="row mb-3">
          <div class="col-2">
            <label id="gender">Genre</label>
          </div>
          <div class="col-10" aria-labelledby="gender">
            ${genders}
          </div>
        </div>
        <div class="row mb-3">
          <div class="col">
            <label for="firstname">Prénom</label>
            <input type="text" class="form-control" id="firstname" placeholder="Rémi">
          </div>
          <div class="col">
            <label for="lastname">Nom</label>
            <input type="text" class="form-control" id="lastname" placeholder="Rubis">
          </div>
        </div>
        <div class="form-group mb-3">
          <label for="email">Email</label>
          <input type="email" class="form-control" id="email" placeholder="contact@email.fr">
        </div>
        <div class="row mb-4">
          <div class="col">
            <label for="promotion">Classe</label>
            <select class="form-select" aria-labelledby="promotion" id="promotion">
              <option value="" selected disabled>Choisir une classe</option>
              <option value="1">B1</option>
              <option value="2">B2</option>
              <option value="3">B3</option>
            </select>
          </div>
          <div class="col">
            <label for="speciality">Spécialité</label>
            <select class="form-select" aria-labelledby="speciality" id="speciality">
              <option value="" selected disabled>Choisir une spécialité</option>
              <option value="1">Développeur WEB</option>
              <option value="2">Marketing digital</option>
              <option value="3">Webdesigner</option>
            </select>
          </div>
        </div>
        <div class="form-group mb-3 col-3">
          <label for="birthdate">Date de naissance</label>
          <input type="date" class="form-control" id="birthdate">
        </div>
        <div class="mb-3">
          <label for="picture" class="form-label">Ajouter une photo de profil (optionnel)</label>
          <input class="form-control" type="file" id="picture" accept=".jpg, .jpeg, .png, .gif">
        </div>
        <h4 class="mb-4">Sécurité</h4>
        <div class="row mb-3">
          <div class="col">
            <label for="password">Mot de passe</label>
            <input type="password" class="form-control" id="password" placeholder="******">
          </div>
          <div class="col">
            <label for="passwordRepeat">Confirmez votre mot de passe</label>
            <input type="password" class="form-control" id="passwordRepeat" placeholder="******">
          </div>
        </div>
        <div class="form-check mb-3">
          <input type="checkbox" class="form-check-input" id="cgu" value="">
          <label class="form-check-label" for="cgu">J'accepte les conditions générales d'utilisations</label>
        </div>
        <button type="submit" class="btn btn-primary">Rejoindre la plateforme</button>
      </form>
    `;
  }

  renderError(fields) {
    if (document.getElementById('errorMsg')) {
      document.getElementById('errorMsg').remove();
    }
    let errors = '';
    fields.forEach((field) => {
      errors += `<li>${field}</li>`;
    });
    return `
    <div class="alert alert-danger mt-4" role="alert" id="errorMsg">
      Une erreur est survenue sur les champs suivants :
      <ul class="mb-0">
        ${errors}
      </ul>
    </div>
    `;
  }

  renderGenders(name, value, isFirst) {
    return `
      <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="genders" id="${value}" value="${value}" ${isFirst ? 'checked' : ''}>
        <label class="form-check-label" for="${value}">${name}</label>
      </div>`;
  }

  registerFormSubmitEvent() {
    document.forms.register.onsubmit = (e) => {
      e.preventDefault();
      this.genders.forEach((gender) => {
        document.getElementById(gender.value).classList.remove('is-valid');
        document.getElementById(gender.value).classList.remove('is-invalid');
      });
      this.getFormData();
    };
  }

  fileValidation(file) {
    const filePath = file.name;

    const allowedExtensions = /(\.jpeg|\.jpg|\.png|\.gif)$/i;

    if (!allowedExtensions.exec(filePath)) {
      return false;
    }
    return true;
  }

  async getFormData() {
    const errorFields = [];

    const data = {
      firstname: document.querySelector('#firstname'),
      lastname: document.querySelector('#lastname'),
      gender: document.querySelector('input[name="genders"]:checked'),
      email: document.querySelector('#email'),
      promotion: document.querySelector('#promotion'),
      speciality: document.querySelector('#speciality'),
      birthdate: document.querySelector('#birthdate'),
      picture: document.querySelector('#picture'),
      password: document.querySelector('#password'),
      passwordReapeat: document.querySelector('#passwordRepeat'),
      cgu: document.querySelector('#cgu')
    };
    Object.entries(data).forEach((entry) => {
      if (entry[1].value === '' && !entry[1].checked && entry[0] !== 'picture') {
        errorFields.push(document.querySelector(`label[for="${entry[1].id}"]`).innerText);
        entry[1].classList.remove('is-valid');
        entry[1].classList.add('is-invalid');
        return 1;
      }
      if (entry[0] === 'picture') {
        entry[1].classList.remove('is-valid');
        entry[1].classList.remove('is-invalid');
      }
      if (entry[0] === 'picture' && entry[1].files[0] && !this.fileValidation(entry[1].files[0])) {
        errorFields.push('Photo de profil (format de fichier incorrect, format autorisés : jpg, jpeg, png, gif)');
        entry[1].classList.remove('is-valid');
        entry[1].classList.add('is-invalid');
        return 1;
      }
      if (entry[0] !== 'picture' || entry[1].value !== '') {
        entry[1].classList.remove('is-invalid');
        entry[1].classList.add('is-valid');
      }
      return 1;
    });

    if (data.password.value !== data.passwordReapeat.value) {
      data.password.classList.remove('is-valid');
      data.passwordReapeat.classList.remove('is-valid');
      data.password.classList.add('is-invalid');
      data.passwordReapeat.classList.add('is-invalid');
      errorFields.push('Les mots de passes ne correspondent pas');
    }

    if (errorFields.length !== 0) {
      document.getElementById('error').innerHTML = this.renderError(errorFields);
      return;
    }

    const payload = {
      firstname: data.firstname.value,
      lastname: data.lastname.value,
      gender: data.gender.value,
      email: data.email.value,
      promotion: data.promotion.value,
      speciality: data.speciality.value,
      birthdate: data.birthdate.value,
      password: this.db.hash(data.password.value)
    };

    if (data.picture.value !== '') {
      const picture = document.getElementById('picture').files[0];
      payload.picture = await this.uploadImgToServer(picture).then((res) => res.filename);
    }

    this.db.addData('students', await payload);

    localStorage.setItem('user', `${payload.firstname} ${payload.lastname}`);
    this.router.pushTo('/');
  }

  async uploadImgToServer(file) {
    const fd = new FormData();
    fd.append('picture', file);
    const data = await fetch('http://localhost:3000/upload', {
      method: 'POST',
      body: fd
    })
      .then((res) => res.json())
      .catch((err) => err);
    return data;
  }

  run() {
    this.app.innerHTML = this.renderPage();
    this.registerFormSubmitEvent();
  }
}
