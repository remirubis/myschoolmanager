import CardComponent from '../components/Card';
import NoPicture from '../assets/default.png';

export default class Students {
  constructor(db) {
    this.app = document.getElementById('content');
    this.db = db;
    this.run();
  }

  importAll(r) {
    const images = {};
    r.keys().map((item) => {
      images[item.replace('./', '')] = r(item);
      return 1;
    });
    return images;
  }

  async run() {
    const images = this.importAll(require.context('../assets/public/', false, /\.(png|jpe?g|gif)$/));
    this.app.innerHTML = `
      <h2>Étudiants</h2>
      <div class="row">
        ${await this.db.getAllData('students').then((res) => res.map((user) => `<div class="col-6 col-xl-3 col-md-4 mb-3">
        <a href="/etudiant/${user.id}" class="text-decoration-none">
      ${new CardComponent(
    `${user.firstname} ${user.lastname}`,
    `<img src="${user.picture ? images[user.picture].default : NoPicture}" class="img-fluid rounded-start w-100" alt="Photo de profil de ${user.firstname} ${user.lastname}">`
  ).display()}
      </a>
    </div>`).join(''))}
      </div>
    `;
  }
}
