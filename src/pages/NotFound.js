export default class NotFound {
  constructor() {
    this.app = document.getElementById('content');
    this.run();
  }

  run() {
    this.app.innerHTML = '<h1>Not Found</h1>';
  }
}
