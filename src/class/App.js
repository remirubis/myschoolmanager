import External from './External';
import Router from './Router';

export default class App {
  constructor() {
    this.app = document.querySelector('#app');
    this.external = new External('https://vzma875a95.execute-api.eu-west-3.amazonaws.com/my-school-manager-api', 'vzma875a95.execute-api.eu-west-3.amazonaws.com', 'mc5mlTUkKR399313HsNFO6zzGhmm5zeQ3slSm72R');
    this.run();
  }

  /*
   * Return HTML of header parts
   *
   * @return {String} Return HTML code who display in front
   *
   */
  renderHeader() {
    return ` 
      <header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
          <div class="container-fluid">
            <a class="navbar-brand router" href="/">MySchoolManager</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
              aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse gap-4" id="navbarSupportedContent">
              <ul class="navbar-nav ms-auto mb-2 mb-lg-0 gap-4">
                <li class="nav-item">
                  <a class="nav-link active router" aria-current="page" href="/">Accueil</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link router" href="/etudiants">Etudiants</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link router" href="/promotions">Promotions</a>
                </li>
              </ul>
              <form class="d-flex">
                <input class="form-control me-2" type="search" placeholder="Rechercher" aria-label="Search">
                <button class="btn btn-secondary" type="submit"><i class="fa-solid fa-magnifying-glass"></i></button>
              </form>
            </div>
          </div>
        </nav>
      </header>
    `;
  }

  renderFooter() {
    return '';
  }

  addRouterEvent() {
    document.body.onclick = (e) => {
      if (e.target.className && e.target.className.indexOf('router') !== -1) {
        const router = new Router(this.external);
        router.route(e);
        e.preventDefault();
      }
    };
  }

  /*
   * Running app with all components like header, contacts and chat.
   */
  async run() {
    this.app.innerHTML += this.renderHeader();
    this.app.innerHTML += '<main id="content" class="w-75 mx-auto py-5"></main>';
    this.app.innerHTML += this.renderFooter();

    this.addRouterEvent();
    return new Router(this.external);
  }
}
