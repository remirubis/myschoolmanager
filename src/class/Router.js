import PageNotFound from '../pages/NotFound';
import HomePage from '../pages/Homepage';
import Login from '../pages/Login';
import Register from '../pages/Register';
import Students from '../pages/Students';

export default class Routes {
  constructor(db) {
    this.routes = {
      '/404': PageNotFound,
      '': HomePage,
      '/': HomePage,
      '/login': Login,
      '/register': Register,
      '/etudiants': Students
    };

    window.route = this.route;
    this.db = db;
    this.handleLocation();
  }

  async handleLocation() {
    const path = window.location.pathname;
    let Route = {};

    if (!localStorage.getItem('user') && this.routes[path]) {
      if (path === '/register') {
        Route = this.routes['/register'];
        return new Route(this.db, this);
      }
      window.history.pushState({}, '', '/login');
      Route = this.routes['/login'];
      return new Route(this.db, this);
    }

    if (this.routes[path]) {
      if (path === '/login' || path === '/register') {
        window.history.pushState({}, '', '/');
        Route = this.routes['/'];
        return new Route(this.db, this);
      }
      Route = this.routes[path];
      return new Route(this.db, this);
    }
    window.history.pushState({}, '', '/404');
    Route = this.routes['/404'];
    return new Route(this.db, this);
  }

  pushTo(route) {
    window.history.pushState({}, '', route);
    this.handleLocation();
  }

  route(event) {
    const ev = event || window.event;
    event.preventDefault();
    window.history.pushState({}, '', ev.target.href);
    this.handleLocation();
  }
}
