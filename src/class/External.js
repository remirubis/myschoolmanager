import axios from 'axios';

const md5 = require('md5');

export default class External {
  constructor(address, host, apiKey) {
    this.address = address;
    this.headers = {
      host,
      headers: {
        Accept: '*/*',
        'Content-Type': 'application/json;charset=utf-8',
        'x-api-key': [apiKey]
      }
    };
  }

  async getAllData(dataRoute) {
    return axios.get(`${this.address}/${dataRoute}/view`, this.headers)
      .then((res) => res.data)
      .catch((err) => err);
  }

  async getSingleData(dataRoute, id) {
    return axios.get(`${this.address}/${dataRoute}/view?id=${id}`, this.headers)
      .then((res) => res.data)
      .catch((err) => err);
  }

  async getSize(dataRoute) {
    return this.getAllData(dataRoute).then((res) => res.length);
  }

  async sortDataBy(dataRoute, option) {
    const sort = await this.getAllData(dataRoute).then(
      (res) => res.sort((a, b) => b[option] - a[option])
    );
    return sort;
  }

  async addData(dataRoute, body) {
    return axios.post(`${this.address}/${dataRoute}/add`, JSON.stringify(body), this.headers)
      .then((res) => res)
      .catch((err) => err);
  }

  async updateData(dataRoute, id, body) {
    return axios.patch(`${this.address}/${dataRoute}/update?id=${id}`, JSON.stringify(body), this.headers)
      .then((res) => res)
      .catch((err) => err);
  }

  async deleteData(dataRoute, id) {
    return axios.delete(`${this.address}/${dataRoute}/delete?id=${id}`, this.headers)
      .then((res) => res)
      .catch((err) => err);
  }

  hash(rawPassword) {
    let hashed = md5(`${rawPassword}dswfl19#`);
    for (let i = 0; i <= 10; i += 1) {
      hashed = md5(hashed);
    }
    return hashed;
  }
}
