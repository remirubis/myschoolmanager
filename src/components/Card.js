/*
* Create a card for displaying basic information with an header, a body and a footer
*
* @param {String} title Set the title of the card
* @param {String} content Set the content of the card
* @param {String} footer Set the footer content of the card
*
* @return {String} Return a card element
*
*/
export default class Card {
  constructor(title, content, footer) {
    this.title = title;
    this.content = content;
    this.footer = footer;
  }

  display() {
    return `
    <div class="card">
      <div class="card-header fw-bold">
        ${this.title}
      </div>
      <div class="card-body p-0">
        ${this.content}
      </div>
      ${this.footer ? `<div class="card-footer">
        ${this.footer}
      </div>` : ''}
    </div>
  `;
  }
}
