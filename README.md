# MySchoolManager

## Description

The realization of this project is framed in the learning of no-sql databases. In my case I would use a DynamoDB database (AWS) by sending my data to the API Gateway service of AWS and executing a lambda function which takes care of creating, displaying, deleting or updating the data present in the database.

**Technical constraints imposed for this project :**

- Using **AWS** (DynamoDB, API Gateway, Lambda)
- Using **Bootstrap 5** for styling

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install this project.

Use node v16 use :
```bash
nvm install 16
```

```bash
npm i
```

## Launch local server API for pictures management

```bash
cd server && node index.js
```

## Launch front-end

Start the application with :

```bash
npm run start
```

Created the dist with :

```bash
npm run dist
```

Analyse the coding rules with :

```bash
npm run lint
```

## :bow: Author

:bust_in_silhouette: [Remi RUBIS](https://gitlab.com/remirubis)

## :heart: Special thanks

:bust_in_silhouette: [Axel Pion](https://gitlab.com/Maengdok)  
:bust_in_silhouette: [Quentin Aubert](https://gitlab.com/quentin.aubert)  
:bust_in_silhouette: [David Pinho](https://gitlab.com/Akalmie)
